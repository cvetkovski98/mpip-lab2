package com.example.lab2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.lab2.data.FakeApi
import com.example.lab2.data.adapters.MovieAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_movie_list.*


class MovieListFragment : Fragment() {
    private lateinit var api: FakeApi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.api = FakeApi.instance
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val movieAdapter = MovieAdapter(api.movies) { movieId -> onMovieDetailsClick(movieId) }
        val llm = LinearLayoutManager(this.context)

        recycle_view_container.layoutManager = llm
        recycle_view_container.adapter = movieAdapter


        val fab: FloatingActionButton = view.findViewById(R.id.fab)
        fab.setOnClickListener { v -> onFabClick(v) }
    }

    private fun onFabClick(view: View) {
        val action = MovieListFragmentDirections.actionMovieListFragmentToAddNewMovieFragment()
        findNavController().navigate(action)
    }

    private fun onMovieDetailsClick(movieId: Int) {
        val action = MovieListFragmentDirections.actionMovieListFragmentToMovieDetailsFragment(movieId)
        findNavController().navigate(action)
    }
}