package com.example.lab2.data.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.lab2.R
import com.example.lab2.data.models.Movie

class MovieAdapter(
    private val movies: MutableList<Movie>,
    private val onClick: (Int) -> Unit
) :
    RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.fragment_movie_item, parent, false)
        return MovieViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = movies[position]
        holder.bind(movie)
    }

    override fun getItemCount() = movies.size

    class MovieViewHolder(
        itemView: View,
        val onClick: (Int) -> Unit
    ) : RecyclerView.ViewHolder(itemView) {
        private val idTextView: TextView = itemView.findViewById(R.id.movieIdText)
        private val movieNameTextView: TextView = itemView.findViewById(R.id.movieNameText)
        private val directorNameTextView: TextView = itemView.findViewById(R.id.directorNameText)
        private var currentMovieId: Int? = null

        init {
            itemView.setOnClickListener {
                currentMovieId?.let {
                    onClick(it)
                }
            }
        }

        fun bind(movie: Movie) {
            idTextView.text = itemView.context.getString(R.string.item_movie_id, movie.id)
            movieNameTextView.text = movie.name
            directorNameTextView.text = movie.director.toString()
            currentMovieId = movie.id
        }
    }
}