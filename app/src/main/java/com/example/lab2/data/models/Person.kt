package com.example.lab2.data.models

class Person(
        var name: String,
        val lastName: String
) {
    var id: Int
        private set

    init {
        this.id = indexer++
    }

    companion object {
        var indexer = 0
    }


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Person) return false

        if (id != other.id) return false
        if (name != other.name) return false
        if (lastName != other.lastName) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + lastName.hashCode()
        return result
    }

    override fun toString(): String {
        return "$name $lastName"
    }

}