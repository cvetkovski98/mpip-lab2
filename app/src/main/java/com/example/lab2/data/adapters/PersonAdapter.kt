package com.example.lab2.data.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.lab2.R
import com.example.lab2.data.models.Person

class PersonAdapter(
    private val people: MutableList<Person>,
    private val onClick: (Int) -> Unit
) :
    RecyclerView.Adapter<PersonAdapter.PersonViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.fragment_person_item, parent, false)
        return PersonViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        val person: Person = people[position]
        holder.bind(person)
    }

    override fun getItemCount() = people.size

    class PersonViewHolder(
        itemView: View,
        onClick: (Int) -> Unit
    ) : RecyclerView.ViewHolder(itemView) {
        private val personNameTextView: TextView = itemView.findViewById(R.id.person_tostring)
        private var currentPersonId: Int? = null

        init {
            itemView.setOnClickListener {
                currentPersonId?.let(onClick)
            }
        }

        fun bind(person: Person) {
            personNameTextView.text = person.toString()
            currentPersonId = person.id
        }
    }

}