package com.example.lab2.data.models

class Movie(
    var name: String,
    var description: String,
    var director: Person,
    var actors: MutableList<Person>
) {
    var id: Int
        private set

    init {
        this.id = indexer++
    }

    companion object {
        var indexer = 0
    }


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Movie) return false

        if (name != other.name) return false
        if (director != other.director) return false
        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + director.hashCode()
        result = 31 * result + actors.hashCode()
        result = 31 * result + id
        return result
    }

    override fun toString(): String {
        return "Movie(name='$name', desctription='$description', director=$director, actors=$actors, id=$id)"
    }

}