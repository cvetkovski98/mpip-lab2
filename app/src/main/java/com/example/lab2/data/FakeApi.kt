package com.example.lab2.data

import com.example.lab2.data.models.Movie
import com.example.lab2.data.models.Person

class FakeApi private constructor() {
    val movies: MutableList<Movie>

    init {
        var actorIndexer = 0
        this.movies = mutableListOf()
        for (i in 1..7) {
            val actorList = mutableListOf<Person>()
            for (j in 1..6) {
                actorList.add(Person("Name ${actorIndexer++}", "Last"))
            }
            this.movies.add(
                Movie(
                    "Movie $i",
                    Constants.dummyText,
                    Person("Director $i", "Last"),
                    actorList
                )
            )
        }
    }

    fun addMovie(movie: Movie) = this.movies.add(movie)

    fun getMovie(id: Int) = this.movies[id]

    fun getActorsForMovieWithId(id: Int) = this.movies[id].actors

    companion object {
        val instance: FakeApi = FakeApi()
    }
}