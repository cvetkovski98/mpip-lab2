package com.example.lab2

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lab2.data.FakeApi
import com.example.lab2.data.adapters.PersonAdapter
import com.example.lab2.data.models.Movie
import com.example.lab2.data.models.Person
import kotlinx.android.synthetic.main.fragment_add_new_movie.*

class AddNewMovieFragment : Fragment() {

    private val api: FakeApi = FakeApi.instance
    private lateinit var actorList: MutableList<Person>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_new_movie, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        actorList = mutableListOf()

        add_movie_recycler_view.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = PersonAdapter(actorList) { i -> onActorRemove(i) }
        }

        add_actor_button.setOnClickListener { _ ->
            onActorAdd()
        }

        save_movie_button.setOnClickListener { _ ->
            onMovieSave()
        }
    }

    private fun onActorAdd() {
        val actorName: Editable? = add_movie_actors_name_edit.text
        val actorLastName: Editable? = add_movie_actors_lastname_edit.text

        if (actorName.isNullOrBlank() || actorLastName.isNullOrBlank()) {
            Toast.makeText(
                this.context,
                "Actor's name and/or last name cannot be blank",
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        val person = Person(actorName.toString(), actorLastName.toString())

        actorList.add(person).also {
            add_movie_recycler_view.adapter?.notifyDataSetChanged()
        }

        actorName.clear()
        actorLastName.clear()
    }

    private fun onMovieSave() {
        val directorName: Editable? = add_movie_director_name_edit.text
        val directorLastName: Editable? = add_movie_director_lastname_edit.text
        val movieName: Editable? = add_movie_name_edit.text
        val movieDescription: Editable? = add_movie_description_edit.text

        if (movieName.isNullOrBlank() || movieDescription.isNullOrBlank()) {
            Toast.makeText(
                this.context,
                "Movie's name and/or description cannot be blank",
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        if (directorName.isNullOrBlank() || directorLastName.isNullOrBlank()) {
            Toast.makeText(
                this.context,
                "Director's name and/or last name cannot be blank",
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        if (actorList.size <= 0) {
            Toast.makeText(
                this.context,
                "Movie's actor list must contain at least one actor",
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        val director = Person(directorName.toString(), directorLastName.toString())

        val movie = Movie(
            movieName.toString(),
            movieDescription.toString(),
            director,
            this.actorList
        )

        api.addMovie(movie)

        val action = AddNewMovieFragmentDirections.actionAddNewMovieFragmentToMovieListFragment()
        findNavController().navigate(action)
    }

    private fun onActorRemove(i: Int) {
        val actorToRemove: Person = actorList.first { p -> p.id == i }
        actorList.remove(actorToRemove).also {
            add_movie_recycler_view.adapter?.notifyDataSetChanged()
        }
    }
}