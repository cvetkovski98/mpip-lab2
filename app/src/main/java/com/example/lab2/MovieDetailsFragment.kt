package com.example.lab2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lab2.data.FakeApi
import com.example.lab2.data.adapters.PersonAdapter
import com.example.lab2.data.models.Movie
import kotlinx.android.synthetic.main.fragment_movie_details.*

class MovieDetailsFragment : Fragment() {
    private val args: MovieDetailsFragmentArgs by navArgs()
    private val api = FakeApi.instance

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val movieId = args.movieId
        loadMovieData(movieId)

    }

    private fun loadMovieData(movieId: Int?) {
        val movie: Movie? = movieId?.let { api.getMovie(it) }
        movie_details_id.text = getString(R.string.item_movie_id, movie?.id)
        movie_details_name.text = movie?.name
        movie_details_director.text = movie?.director.toString()
        movie_details_description.text = movie?.description
        people_recycle_view.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = PersonAdapter(api.getActorsForMovieWithId(args.movieId)) { i -> doNothing(i) }
        }
    }

    private fun doNothing(i: Int) = Unit
}